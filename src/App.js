import React, { useEffect, useState } from 'react';
import {Router, Switch, Route, Redirect} from 'react-router-dom';
import history from './history'
import './App.css';
import TitleBlock from './components/views/TitleBlock';
import Menu from './components/views/Menu';
import View from './components/views/View';
import Room from './components/views/Room';
import Login from './components/views/Login';
import AddNewUser from './components/content/AddNewUser';

function App() {

  const [ floor, setFloor ] = useState(0);
  const [ room, setRoom ] = useState(0);
  const [ user, setUser ] = useState();

  useEffect(() => {
    const parsedUser = localStorage.getItem("user")
    setUser(parsedUser)
  }, [])

  useEffect(() => {
    localStorage.setItem("user", user);
  }, [user])

  return (
    <div className="App">
        <TitleBlock setUser={setUser} user={user} />
        <Router history={history}>
          <Menu floor={floor} setFloor={setFloor} user={user}/>
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/login" />} />
            <Route exact path="/login">
              <Login setUser={setUser} />
            </Route>
            <Route path="/floor/:id">
              <View floor={floor} setRoom={setRoom}/>
            </Route>
            <Route path="/room/:id">  
              <Room selectedRoom={room}/>
            </Route>
            <Route path="/addUser">
              <AddNewUser />
            </Route>
          </Switch>
        </Router>

    </div>
  );
}

export default App;
