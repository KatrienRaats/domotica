import { useEffect, useState } from 'react';
import axios from 'axios';
import { useInterval } from './useInterval';

export default function useFloors(){

    const [ loading, setLoading ] = useState(false);
    const [ floors, setFloors ] = useState();


    const getFloors = async () => {
        setLoading(true);
        try{
            const res = await axios.get("http://localhost:5000/floors");
            setFloors(res.data);
        } catch (error) {
            console.log("Could not load data. " + error);
        }
        setLoading(false);
    }

    useInterval(() => {
        reload();
    }, 10000);

    useEffect(() => {
            getFloors();
        },[]);

    const reload = () => getFloors();


    return { loading, floors }
}
