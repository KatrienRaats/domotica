import {useState} from 'react';

export default function useLightElement (initialState){
    const [ fields, setValues ] = useState(initialState);

    return [
        fields,
        function (event){
            setValues({
                ...fields,
                [event.target.id]: parseInt(event.target.value)
            })
        }
    ]
}