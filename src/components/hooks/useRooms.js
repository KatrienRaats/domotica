import { useEffect, useState } from 'react';
import axios from 'axios';
import { useInterval } from './useInterval';

const useRooms = () => {

    const [ loadingRooms, setLoadingRooms ] = useState(false);
    const [ rooms, setRooms ] = useState();

    const getRooms = async () => {
        setLoadingRooms(true);
        try{
            const res = await axios.get("http://localhost:5000/rooms");
            setRooms(res.data);
        } catch (error) {
            console.log("Could not load rooms. " + error);
        }
        setLoadingRooms(false);
    }

    useInterval(() => {
        reloadRooms();
    }, 10000);


    useEffect(() => {
           getRooms();
        },[]);

    const reloadRooms = () => getRooms();

    return { loadingRooms, rooms, reloadRooms }
}

export default useRooms;
