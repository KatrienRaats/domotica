import {useState} from 'react';

export default function useDarkened (initialState){
    const [ fields, setValues ] = useState(initialState);

    return [
        fields,
        function (event){
            setValues({
                ...fields,
                [event.target.id]: event.target.checked
            })
        }
    ]
}