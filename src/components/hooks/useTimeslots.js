import { useEffect, useState } from 'react';
import axios from 'axios';
import { useInterval } from './useInterval';

export default function useTimeslots(){

    const [ loadingSlots, setLoadingSlots ] = useState(false);
    const [ timeslots, setTimeslots ] = useState();


    const getTimeslots = async () => {
        setLoadingSlots(true);
        try{
            const res = await axios.get("http://localhost:5000/timeslots");
            setTimeslots(res.data);
        } catch (error) {
            console.log("Could not load timeslots. " + error);
        }
        setLoadingSlots(false);
    }

    useInterval(() => {
        reload();
    }, 10000);

    useEffect(() => {
            getTimeslots();
        },[]);

    const reload = () => getTimeslots();


    return { loadingSlots, timeslots, reload }
}
