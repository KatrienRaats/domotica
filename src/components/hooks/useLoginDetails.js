import { useEffect, useState } from 'react';
import axios from 'axios';

export default function useLoginDetails(){

    const [loadingUsers, setLoadingUsers ] = useState();
    const [ users, setUsers ] = useState();


    const getUser = async () => {
        setLoadingUsers(true);
        try{
            const res = await axios.get(`http://localhost:5000/userDetails/`);
            setUsers(res.data);
        } catch (error) {
            console.log("Could not load data. " + error);
        }
        setLoadingUsers(false);
    }

    useEffect(() => {
            getUser();
        },[]);


    return { loadingUsers, users }
}
