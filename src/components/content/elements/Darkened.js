import React from 'react';
import {Form, Col } from 'react-bootstrap';

function Darkened (props){
    
    const {defaultdarkened, changeDarkened} = props;

    return(
        <Form.Row>
            <Col sm="4">
                <Form.Label style={{color: "CornSilk"}}>
                    Turn up the blinds
                </Form.Label>
            </Col>
            <Col sm="4">
                <Form.Check 
                style={{color: "CornSilk"}}
                type="switch"
                id="defaultdarkened"
                checked={defaultdarkened}
                onChange={changeDarkened}
            />
            </Col>
            <Col sm="4">
            <Form.Label style={{color: "CornSilk"}}>
                   Close the blinds
                </Form.Label>
                </Col>
        </Form.Row>
    )
}

export default Darkened;