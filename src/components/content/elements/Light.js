import React from 'react';
import {Form, Col } from 'react-bootstrap';

function Light (props){
    
    const {defaultlight, changeLight} = props;

    return(
        <Form.Row>
            <Col sm="4">
                <Form.Label style={{color: "CornSilk"}}>
                    Dim the light
                </Form.Label>
            </Col>
            <Col sm="4">
                <Form.Control className="mb-2"
                type="range"
                value={defaultlight}
                min={0}
                max={20}  
                id="defaultlight"
                onChange={changeLight}
                />
            </Col>
            <Col sm="4">
            <Form.Label style={{color: "CornSilk"}}>
            Brighten the light to {defaultlight} 
            </Form.Label>
            </Col>
        </Form.Row>
    )
}

export default Light;