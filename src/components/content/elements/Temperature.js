import React from 'react';
import {Form, Col } from 'react-bootstrap';

function Temperature (props){
    
    const {defaulttemp, changeTemp} = props;

    return(
        <Form.Row >
            <Col sm="4">
                <Form.Label style={{color: "CornSilk"}}>
                    Set Temperature
                </Form.Label>
            </Col>
            <Col sm="4">
                <Form.Control className="mb-2"
                type="range"
                value={defaulttemp}
                min={10}
                max={30}  
                id="defaulttemp"
                onChange={changeTemp}
                />
            </Col>
            <Col sm="4">
            <Form.Label style={{color: "CornSilk"}}>
                   Turn up the heat to {defaulttemp} °C
                </Form.Label>
                </Col>
        </Form.Row>
    )
}

export default Temperature;