import React from 'react';
import {Form, Col } from 'react-bootstrap';

function Music (props){
    
    const {defaultmusic, changeMusic} = props;

    return(
        <Form.Row>
            <Col sm="4">
                <Form.Label style={{color: "CornSilk"}}>
                Turn off the music
                </Form.Label>
            </Col>
            <Col sm="4">
                <Form.Control className="mb-2"
                type="range"
                value={defaultmusic}
                min={0}
                max={20}  
                id="defaultmusic"
                onChange={changeMusic}
                />
            </Col>
            <Col sm="4">
            <Form.Label style={{color: "CornSilk"}}>
            Turn on the music to {defaultmusic} Db
            </Form.Label>
            </Col>
        </Form.Row>
    )
}

export default Music;