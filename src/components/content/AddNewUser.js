import React, { useReducer } from "react";
import axios from 'axios';
import { Button, Form } from "react-bootstrap";
import useLoginDetails from '../hooks/useLoginDetails';
import {useHistory} from 'react-router-dom';

export default function AddNewUser() {
    
  const history = useHistory();
    const { users} = useLoginDetails();
    const [newUser, setNewuser ] = useReducer(
        (state, newState) => ({...state,...newState}),
        {
        id:0,
        isAdmin:false,
        username:"",
        password:""

    });

    const addUser = async() => {    
        try{
            const resp = await axios.post(`http://localhost:5000/userDetails`,newUser);
            console.log(resp.data);
           
        } catch (error) {
    
            console.log("Could not add users. " + error);
        }   
    }

    const selectText = e => {
        const {name, value} = e.target;
        setNewuser({[name]: value})
    }

    function handleSubmit(event) {
        event.preventDefault();
        setNewuser({["id"]:users.length +1})
        addUser();
        history.push("/floor/0/floorplan");
      }

      return (
        <div className="addUser">
            {users &&
          <Form style={{margin: "0 auto", maxWidth:"320px"}}>
            <Form.Group controlId="username" >
              <Form.Label>UserName</Form.Label>
              <Form.Control
                autoFocus
                type="username"
                name="username"
                value={newUser.username}
                onChange={selectText}
              />
            </Form.Group>
            <Form.Group controlId="password" >
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={newUser.password}
                name="password"
                onChange={selectText}
                type="password"
              />
            </Form.Group>
            <Form.Group>
            <Button block type="submit" onClick={(e)=>handleSubmit(e)} >
              Add User
            </Button>
            </Form.Group>
          </Form>
        }
        </div>
      );
    }