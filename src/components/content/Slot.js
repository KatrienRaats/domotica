import React, {useState, useReducer} from 'react';
import axios from 'axios';
import {Route,Link, useHistory} from 'react-router-dom';
import AddSlot from './AddSlot';
import nextId from 'react-id-generator';
import {Alert, Button, Col, Row } from 'react-bootstrap';
import { BsFillTrashFill} from 'react-icons/bs';
import useTimeslots from '../hooks/useTimeslots';

const Slot = ({room}) => {

const history = useHistory();
const [show, setShow] = useState(false);

const {loadingSlots, timeslots, reload } = useTimeslots();
const [showAlert, setShowAlert] = useState(false);
const [currentid, setcurrentid ] = useState();

const [slot, setSlot ] = useReducer(
    (state, newState) => ({...state,...newState}),
    {
    id:0,
    roomid:room.id,
    element: "",
    starttijd:"",
    eindtijd:"",
    waarde:""
});

const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const addSlotToList = async() => {    
    try{
        const resp = await axios.post(`http://localhost:5000/timeslots`,slot);
        console.log(resp.data);
        reload();
    } catch (error) {

        console.log("Could not add timeslots. " + error);
    }   
}

const deleteSlot = async(itemid) => {
    try{
        const resp = await axios.delete(`http://localhost:5000/timeslots/${itemid}`);
        console.log(resp.data);
        reload();
       
    } catch (error) {

        console.log("Could not delete timeslots. " + error);
    }
}

const handleSubmit = (e) => {
    e.preventDefault();
    setSlot({["id"]:timeslots.length +1})
    addSlotToList();
    history.push("/room/" + slot.roomid +"/details")
}

return (
    <>
        <Route path="/room/:id/details">
            <Row style={{backgroundColor:"darkslategrey", color:"cornsilk", textAlign:"center"}}>
                <Col>
                {showAlert &&
                    <Alert variant="danger">
                        <Alert.Heading>Ready to add this Timeslot?</Alert.Heading>
                            <p>Sure you want to delete this timeslot?</p>
                        <div className="d-flex justify-content-end">
                            <Button type="button" onClick={(e) => {setShowAlert(false); deleteSlot(currentid); reload()}} variant="outline-danger">
                                Confirm delete
                            </Button>
                            <Button type="button" variant="outline-danger" onClick={(e) => setShowAlert(false)}>
                                Cancel delete
                            </Button>
                        </div>
                    </Alert>
                }
                {!showAlert && 
                <div>
                          
                    {!loadingSlots && timeslots &&
                        timeslots.filter(slot => slot.roomid === room.id).length ? 
                        (timeslots.filter(slot => slot.roomid === room.id).map((filteredSlot) => (
                            filteredSlot.element === "light" ? 
                            <p key={nextId()}><BsFillTrashFill onClick={() => {setShowAlert(true); setcurrentid(filteredSlot.id)}}/> 
                             Lights on from {filteredSlot.starttijd}h to {filteredSlot.eindtijd}h </p>: 
                                (filteredSlot.element === "music" ? 
                                <p key={nextId()}><BsFillTrashFill onClick={() => {setShowAlert(true); setcurrentid(filteredSlot.id)}}/> 
                                 Music on from {filteredSlot.starttijd}h to {filteredSlot.eindtijd}h</p>:
                                    (filteredSlot.element === "darkened" ? 
                                    <p key={nextId()}><BsFillTrashFill onClick={() => {setShowAlert(true); setcurrentid(filteredSlot.id)}}/> 
                                     Blinds down from {filteredSlot.starttijd}h to {filteredSlot.eindtijd}h</p>:
                                        (filteredSlot.element === "temp" ? 
                                            <p key={nextId()}><BsFillTrashFill onClick={() => {setShowAlert(true); setcurrentid(filteredSlot.id)}}/> 
                                            Temperature set on {filteredSlot.waarde}°C from {filteredSlot.starttijd}h to {filteredSlot.eindtijd}h</p>:
                        null)))))):<p style={{color:"cornsilk", fontWeight:"bold"}}>No timeslots added yet!</p>
}                       <Button variant="info" as={Link} to={`/room/${room.id}/addTimeslot`} onClick={handleShow}>Add Timeslot</Button>   
                </div>
                }
                </Col>
            </Row>
        </Route>
        <Route path="/room/:id/addTimeslot">
            <AddSlot setSlot={setSlot} slot={slot} room={room} handleSubmit={handleSubmit} handleClose={handleClose} show={show} />
        </Route>
    </>
)
}

export default Slot;