import React from 'react';
import {Link} from 'react-router-dom';
import { Modal, Button,Form } from 'react-bootstrap';
import nextId from 'react-id-generator';
import TimePicker from 'react-time-picker'

const AddTimeslot = ({room, handleSubmit, setSlot, slot, handleClose, show}) => {
    
    const roomData = Object.keys(room);
    const excl = [0,1,2,3];
    const filtered = roomData.filter((_, i) => !excl.includes(i));

    const selectText = e => {
        const {name, value} = e.target;
        setSlot({[name]: value})
    }

    const selectWaarde = e => {
        const {name, value} = e.target;
        setSlot({[name]: parseInt(value)})
    }

    const selectStart = value => {
        setSlot({["starttijd"]: value}); 
    }

    const selectEnd = value => {
        setSlot({["eindtijd"]: value}); 
    }

    return (
        <>
        {show &&
            <Modal show={show} onHide={handleClose} animation={false}>
                <Modal.Header>
                    <Modal.Title>Add a timeslot</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <Form.Control name="element" value={slot.element} as="select" onChange={selectText} required>
                            <option>Select your element ...</option>
                        {filtered.map((item) => 
                            <option key={nextId()} value={item} >{item}</option>
                        )}
                        </Form.Control>          
                        <Form.Label>Starttime</Form.Label>
                        <TimePicker name="starttijd" value={slot.starttijd} onChange={selectStart} required/>
                        <Form.Label>Endtime</Form.Label>
                        <TimePicker name="eindtijd" value={slot.eindtijd} onChange={selectEnd} required/>
                        <Form.Control placeholder="Enter value" name="waarde" value={slot.waarde} onChange={selectWaarde} required/>
                    </Modal.Body>
                    <Modal.Footer>
                <Button type="Submit" onClick={(e) => handleSubmit(e)}> Submit </Button>
                <Button type="button" as={Link} to={`/room/${room.id}/details`}> Cancel </Button>
                </Modal.Footer>
            </Modal>
        }
        </>
    )
}

export default AddTimeslot;