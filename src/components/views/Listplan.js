import React from 'react';
import useRooms from '../hooks/useRooms';
import {Link} from 'react-router-dom';
import picmusic from '../../assets/music.jpg'
import musicoff from '../../assets/musicoff.jpg';
import './views.css';

const Listplan = ({floor, setRoom, setLight, setDarkened}) => {

const { loadingRooms, rooms } = useRooms();



    return (
        <div>
            <h1 style={{color:"white", textAlign: "center"}}>Listplan</h1>
            <ul className="listCards my-2"
                style={{display:"flex"}}>
            {!loadingRooms && rooms && 
            rooms.filter(room => room.floor === floor).map ((filteredroom) => 
                <li key={filteredroom.id} className="listrooms text-center h-100" 
                    style ={setLight(filteredroom)>0 ? { backgroundColor: "Gold", opacity: (filteredroom.light.defaultlight/20) } : 
                        (setDarkened(filteredroom) ? { backgroundColor: "darkgrey" }:{ backgroundColor: "cornsilk" } )
                        }
                        >
                    <Link
                    style={{color:"black"}}
                    onClick={()=> setRoom(filteredroom)}
                    to={`/room/${filteredroom.id}/details`}>
                        {filteredroom.name} </Link>
                    {filteredroom.hasOwnProperty("music") ? 
                                    (filteredroom.music.defaultmusic > 0 ? <div><img style={{height: "auto", width: "30px" }} src={picmusic} alt="music on" /> </div>: <div><img style={{height: "auto", width: "30px" }} src={musicoff} alt="music off"/></div>): null }
                                    {filteredroom.hasOwnProperty("temp") ? <div className="tempvalue">{filteredroom.temp.defaulttemp} °C</div> : null}
                </li>
        
            )}
            </ul>
        </div>
    )
}

export default Listplan;