import React from 'react';
import axios from 'axios';
import {Form, Row, Col, Button} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import useLightElement from '../hooks/useLightElement';
import useDarkenedElement from '../hooks/useDarkenedElement';
import useMusicElement from '../hooks/useMusicElement';
import useTempElement from '../hooks/useTempElement';
import Light from '../content/elements/Light';
import Darkened from '../content/elements/Darkened';
import Temperature from '../content/elements/Temperature';
import Music from '../content/elements/Music';
import Slot from '../content/Slot';

function Room (props){
    
    const {selectedRoom} = props;
    const history = useHistory();
    const [lightfields, handleLightChange] = useLightElement(selectedRoom.light);
    const [darkenedfields, handleDarkenedChange] = useDarkenedElement(selectedRoom.darkened);
    const [musicfields, handleMusicChange] = useMusicElement(selectedRoom.music);
    const [tempfields, handleTempChange] = useTempElement(selectedRoom.temp);

    const selectNewElements = async (id) => {
        try{
            const resp = await axios.patch(`http://localhost:5000/rooms/${id}`, { light: lightfields, music:musicfields, darkened:darkenedfields,temp:tempfields});
            console.log(resp);
        } catch (error) {
            console.log("Could not update fields. " + error);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        alert("New values saved");
        history.push("/room/"+selectedRoom.id +"/details");
        selectNewElements(selectedRoom.id);

    }

    return(
        <div>
                <div>
            {selectedRoom &&
            <div>
                <h1 style={{color:"white", textAlign:"center", margin:"20px"}}>{selectedRoom.name}</h1>
                <Form className="roomform">
                    <Row>  
                        <Col style={{textAlign:"center"}} xs="12" lg="6">
                            <h2 style={{color:"coral", textAlign:"center", margin:"20px"}}>Details Room</h2>
                            {selectedRoom.hasOwnProperty('light') ? <Light defaultlight={lightfields.defaultlight} changeLight={handleLightChange} />:null}
                            {selectedRoom.hasOwnProperty('darkened') ? <Darkened defaultdarkened={darkenedfields.defaultdarkened} changeDarkened={handleDarkenedChange} />:null}
                            {selectedRoom.hasOwnProperty('temp') ? <Temperature defaulttemp={tempfields.defaulttemp} changeTemp={handleTempChange} />:null}
                            {selectedRoom.hasOwnProperty('music') ? <Music defaultmusic={musicfields.defaultmusic} changeMusic={handleMusicChange} />:null}
                            <Button variant="info" onClick={(e) => handleSubmit(e)} type="submit">Save Changes</Button>
                        </Col>
                        <Col xs="12" lg="6">
                            <h2 style={{color:"coral", textAlign:"center", margin:"20px"}}>Timeslots</h2>
                            <Slot room = {selectedRoom}/>
                        </Col>  
                    </Row>
                </Form>
            </div>}
        </div>
         
        </div>
    )
}


export default Room;