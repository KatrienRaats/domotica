import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import useLoginDetails from "../hooks/useLoginDetails";

export default function Login({setUser}) {
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [ invalidCredentials, setInvalidCredentials ] = useState("");
    const history = useHistory();

    const { users } = useLoginDetails();

    function checkUser(userName, password){
        if (users){
            for ( let user of users ){
                if (user.username === userName && user.password === password){
                    setInvalidCredentials("");
                    setUser(userName);
                    history.push("/floor/0/floorplan");
            } 
            else {
                setUserName('');
                setPassword('');
                setInvalidCredentials("Wrong credentials! Please try again.");
            }
        }
    }
}

    function validateForm() {
      return userName.length > 0 && password.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();
        checkUser(userName, password);
      }

      return (
        <div className="Login">
          <form style={{margin: "0 auto", maxWidth:"320px"}}>
            <Form.Group controlId="username" >
              <Form.Label>UserName</Form.Label>
              <Form.Control
                autoFocus
                type="username"
                value={userName}
                onChange={e => setUserName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="password" >
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={password}
                onChange={e => setPassword(e.target.value)}
                type="password"
              />
            </Form.Group>
            <Form.Group style={{color:"red"}}>
              {invalidCredentials}
            </Form.Group>
            <Form.Group>
            <Button block disabled={!validateForm()} type="submit" onClick={(e)=>handleSubmit(e)} >
              Login
            </Button>
            </Form.Group>
          </form>
        </div>
      );
    }