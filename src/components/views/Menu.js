import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Navbar } from 'react-bootstrap';
import useFloors from '../hooks/useFloors';
import Nav from 'react-bootstrap/Nav';

const Menu = ({floor, setFloor, user}) => {
    const { floors, loading } = useFloors();

    
   
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand>Floors</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
          {!loading && floors && user &&
          <Nav className="mr-auto" variant="tabs"  defaultActiveKey="1" >
            {floors.map((floor) => 
              <Nav.Item key={floor.id}>
                <Nav.Link as={Link} to={`/floor/${floor.id}/floorplan`} onClick={(e) => {setFloor(floor.id)}}>{floor.name} </Nav.Link>
              </Nav.Item>
              )}
          </Nav>}
          {user &&
            <Nav className="justify-content-end">
                <Button disabled={user==="" ? false : true } variant="dark" value='listplan' as={Link} to={`/floor/${floor}/listplan`} >Listplan</Button>
                <Button variant="dark" value='floorplan' as={Link} to={`/floor/${floor}/floorplan`} >Floorplan</Button>
                <Button variant="dark" value='addUser' as={Link} to={`/addUser`}>Add new user</Button>
            </Nav>
}
          </Navbar.Collapse>
        </Navbar>
    )
}

export default Menu;