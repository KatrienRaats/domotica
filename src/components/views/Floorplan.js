import React from 'react';
import {Link} from 'react-router-dom';
import {Container, Card, Row, Col} from 'react-bootstrap';
import useRooms from '../hooks/useRooms';
import picmusic from '../../assets/music.jpg'
import musicoff from '../../assets/musicoff.jpg';

function Floorplan(props){
    const { loadingRooms, rooms } = useRooms();
    const {floor, setRoom,setLight, setDarkened } = props;


    return(
        <div>
           <h1 style={{color:"white", textAlign: "center"}}>Floorplan</h1>
           <Container >
            {!loadingRooms && rooms && 
            rooms.filter(room => room.floor === floor).map ((filteredroom) => 
                <div key={filteredroom.id} 
                    style={{position: "absolute", top: filteredroom.coordinates.x, left: filteredroom.coordinates.y, 
                            height: filteredroom.coordinates.height, width: filteredroom.coordinates.width, 
                            border: "2px solid gold", backgroundColor:"cornsilk"}}
                    >
                    <Card  
                    className="roomcard text-center h-100" 
            
                    style ={setLight(filteredroom)>0 ? { backgroundColor: "Gold", opacity: (filteredroom.light.defaultlight/20) } : 
                        (setDarkened(filteredroom) ? { backgroundColor: "darkgrey" }:{ backgroundColor: "cornsilk" } )
                        }
                    onClick={()=> setRoom(filteredroom)}
                    as={Link}
                    to={`/room/${filteredroom.id}/details`}
                    >
                        <Card.Header className="cardheader">{filteredroom.name}</Card.Header>
                            <Card.Body as={Row}>
                                <Col xs="5">
                                {filteredroom.hasOwnProperty("music") ? 
                                    (filteredroom.music.defaultmusic > 0 ? <Card.Img style={{height: "auto", width: "50px" }} src={picmusic} /> : <Card.Img style={{height: "auto", width: "50px" }} src={musicoff} />): null }
                                </Col>
                                <Col xs="7">
                                {filteredroom.hasOwnProperty("temp") ? <div className="tempvalue">{filteredroom.temp.defaulttemp} °C</div> : null}
                                </Col>
                            </Card.Body>
                    </Card>
                </div>
            )}
            </Container>
        </div>
    )
}

export default Floorplan;