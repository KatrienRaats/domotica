import React from 'react';
import titleImage from '../../assets/titleblock.jpg';
import {Row, Col} from 'react-bootstrap';

const TitleBlock = ({user, setUser, setSignedIn, signedIn}) => {
    
    function singOut(e){
        setUser('');
    }

    return(
        <div style={{color:"white", textAlign:"center", backgroundImage: `url(${titleImage}`, paddingTop: "30px", paddingBottom:"30px"}}>
            <Row>
                <Col sm="9">
                    <h1>Welcome to Smart @ Home</h1>
                </Col>
                {user &&
                <Col sm="2">
                    <p style={{textAlign:"right"}} >{user}</p>
                    <p style={{textAlign:"right"}}><a href="http://localhost:3000/login" onClick={(e) => singOut(e)}>Sign out</a></p>
                </Col>
                }
            </Row>
        </div>
    )
}

export default TitleBlock;