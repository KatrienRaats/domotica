import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Listplan from './Listplan';
import Floorplan from './Floorplan';

const View = ({floor, setRoom}) => {
    
    function setLight(filteredRoom){
        return filteredRoom.light.defaultlight;
    }
    
    function setDarkened(filteredRoom){
        if (filteredRoom.hasOwnProperty("darkened"))
        {
        return filteredRoom.darkened.defaultdarkened;
        } else {
            return null;
        }
    }    
return (
    <Switch>
        <Route path="/floor/:id/listplan">
            <Listplan floor={floor} setRoom={setRoom} setLight={setLight} setDarkened={setDarkened}/>
        </Route>
        <Route path="/floor/:id/floorplan">
            <Floorplan floor={floor} setRoom={setRoom} setLight={setLight} setDarkened={setDarkened}/>
        </Route>
    </Switch>
    )
}

export default View;